FROM requarks/wiki:2.5.288

USER root

RUN chgrp -R 0 /wiki /logs && \
    chmod -R g=u /wiki /logs && \
    chmod g=u /etc/passwd

ADD run.sh /run.sh
RUN chmod +x /run.sh

USER 1001

EXPOSE 3000
ENV CONFIG_FILE /config/config.yml
VOLUME /uploads
VOLUME /config

CMD ["/run.sh"]
