#!/bin/sh

# this part adds the current user to /etc/passwd as per:
# https://docs.okd.io/latest/creating_images/guidelines.html#use-uid

if ! whoami &> /dev/null; then
  if [ -w /etc/passwd ]; then
    echo "${USER_NAME:-wikijs}:x:$(id -u):0:${USER_NAME:-wikijs} user:${HOME}:/sbin/nologin" >> /etc/passwd
  fi
fi

node server

